const mongoose = require('mongoose')

const appreciationSchema = new mongoose.Schema({
  data: String,
  date: Date
})

const appreciationModel = mongoose.model('appreciation', appreciationSchema)
module.exports = {appreciationSchema, appreciationModel}