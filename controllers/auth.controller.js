const volunteerModel = require('../models/volunteers.model')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

function signup(req, res) {
  /*
    req.body = {
      name, 
      pwd, 
      email
    }
  */

    const hashed_pwd = bcrypt.hashSync(req.body.pwd, 10)

    const hashed_body = {
      name: req.body.name,
      email: req.body.email,
      pwd: hashed_pwd
    }

    volunteerModel.create(hashed_body)
      .then((volunteer) => {

        const insideToken = {
          name: volunteer.name,
          id: volunteer._id,
          email: volunteer.email
        }

        const token = jwt.sign(
          insideToken,
          process.env.SECRET
        )

        const resVolunteer = {
          id: volunteer._id,
          name: volunteer.name,
          email: volunteer.email,
          token: token
        }

        res.json(resVolunteer)
      })
      .catch((err) => {
        res.json(err)
      })
}

function login(req, res) {
  /*
    req.body = {
      email,
      pwd
    }
  */

  volunteerModel.findOne({ email: req.body.email })
    .then((volunteer) => {
      if (!volunteer) res.json('Wrong email')

      bcrypt.compare(
        req.body.pwd, 
        volunteer.pwd, 
        (err, result) => {
          if (err) res.json('Wrong password')
          
          const insideToken = {
            name: volunteer.name,
            email: volunteer.email,
            id: volunteer._id
          }
          const token = jwt.sign(
            insideToken,
            process.env.SECRET,
          )

          resVolunteer = {
            name: volunteer.name,
            email: volunteer.email,
            id: volunteer._id,
            token: token
          }
          
          res.json(resVolunteer)
        })

    })
    .catch((err) => {
      console.log(err)
    })
}

module.exports = {
  signup,
  login
}