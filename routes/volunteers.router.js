const jwt = require('jsonwebtoken')
const volunteerRouter = require('express').Router()

const { getAllVolunteers, getVolunteer } = require('../controllers/volunteers.controller')

volunteerRouter.get( '/' , getAllVolunteers)
volunteerRouter.get( '/me', auth, getVolunteer)

function auth(req, res, next) {
  /*
    req.body
    req.query
    req.params
    req.headers // req.headers.token
  */

  jwt.verify(
    req.headers.token, 
    process.env.SECRET, 
    (err, insideToken) => {
      if (err) res.json('Token not valid')
      res.locals.id = insideToken.id
      next()
  })
}

module.exports = volunteerRouter